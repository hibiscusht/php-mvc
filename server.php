<?php

require "config.php";

$comp = explode('/',$_SERVER['REQUEST_URI']);
//var_dump($comp);
echo '<hr>';

$name = $comp[2];
$method = !isset($comp[3]) ? 'index' : $comp[3];
  
spl_autoload_register(function($name){
    require $name . '.php';
});

$class = "controllers\\$name";
$obj = new $class();
$obj->$method();