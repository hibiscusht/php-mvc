<?php

namespace orm;

class db {
    protected $conn;
    protected $model;
    public function __construct($model)
    {
        $this->conn = mysqli_connect(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
        $this->model = $model;
    }
    public function findAll()
    {
        $qry = mysqli_query($this->conn,"SELECT * FROM $this->model");
        $result = [];
        while($data = mysqli_fetch_object($qry)){
            array_push($result,$data);
        }    
        return $result;
    }
    public function findOne($id)
    {
        $qry = mysqli_query($this->conn,"SELECT * FROM $this->model WHERE id = $id");
        $result = [];
        while($data = mysqli_fetch_object($qry)){
            array_push($result,$data);
        }    
        return $result;
    }
}